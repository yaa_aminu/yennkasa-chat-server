Pairapp-Instant Messenger - re-imagining instant messaging from the ground app ### How do I get set up? ###

The Pairapp Instant Messenger Protocol version 1
================================================

The pairapp instant messenger protocol defines set of rules by which clients who want to communicate with the server and other clients must abide. An important concept to keep in mind is that the server is completely agnostic of the message beign passed around. In other words its just a hub that connects clients together and allow them to interact by relaying messages from one client to onother client. For this to happen the server needs to know how to relay a particular message. Clients therefore are required to prepend at least a one byte header, a variable length byte array signifying the reciepient of the message delimited by the ascii character code for **'-'** which is 45 to the message beign sent(payload). Based on this header, The server determines what to do with the message. The recipeint will not recieve this header. only the payload is relayed to the other recipient on the other end.  
  
**Important:** Any client that sends a message that does not abide by this protocol will be disconnected immediately with no reason. The client will just be disconnected.  
Also this server only handle messages in network byte order (Big endian)

#### Authtentication

During registration you must request for a json web token from the registration srever after a successful sign up or login.You must always submit this token during the handshake to the server in the http header field 'authorization'. This is case-insensitive. so 'Authorization' and 'authorization' are the same. If you fail authentication, you will be disconnected with no reason. This token must be refreshed every 15 days

#### Header format

*   **Part One**  
    A one byte with one of the following values.(values can be ORed when it makes sense)
    *   0x1 == dont persist or even send (through push notifications) the payload if the client is offline. To be precise, only send this payload to the reciepient when its online otherwise drop the payload.
    *   0x2 == send but don't persist the payload(even if reciepient is offline) .But the payload may be sent to the reciepint via push if it's offline
    *   0x4 == always persist the payload before even attempting to send the payload to the recipeint. if the recipient is offline the server will send the payload via push notification. when using this option, the server will decide how to hint the reciepient. if the payload exceeds 1KB, only a hint(format of the hint is defined below) will be sent to the reciepint when its offline. upon recieving this hint, the client must wake up and connect to the server and load the acutal data that was persisted on the server. See notes below for important information
    *   0x8 == if this flag is set, the server will send all the payload to the client via push if the client is offline (not just a hint). Do be aware that still the entire payload is capped at 4KB. If a payload exceeds this the server will not heed to your advice and will go ahead to send a hint to load the actual data. seen notes below for important information
    *   0x10 == if this flag is set in the header, it signifies a very special message. Depending on the payload, this messages orders the server to do one of the following.
        *   start monitoring a particular client on behalf of this client
        *   stop monitoring a particular client on behalf of this client
        *   tell the server that this client is online
        *   tell the server that this client is offlineThe payload of this message is different from others as it is specified by the server.The format of the payload is described below
*   **Part Two**  
    A variable length byte array that is interperated as follows:
    *   if it's length is less or equal to 8 bytes, it is interperated as a number otherwise as a utf-8 encoded string. There is no exception whatsoever. This has a very important implication in the sense that, you cannot name a user with an indentifier that cannot be coalesced to number with length less than 8 bytes. This is enforced by the server that handles registration of users so the chat server can determistically rely on it. a payload will be dropped if this field results in falsy value like empty string(after trim()ming ) or the number 0 and as usual the client will be disconnected immediately. **_The only exception is when the client want to annouce to the server they are online or offline. In this case, the recipient field is not required._**
*   **Part three**  
    exactly one byte holding the dash (-) character. This serves as a delimiter for the header.You must provide this even if the reciepient field is empty.

#### Payload format

The payload follows. The payload has no special encoding whatsoever. The server just treat it as an opaque stream of bytes. The payload must be at least one byte long and at most 16KB long. Be aware that if your payload exceeds 4KB, you cannot dictate to the server that it should send the entire payload via push. The server will only send a hint. if the payload is not available, the message is regarded as invalid and such a misbehaving client will be **disconnected immediately**.  

If the 0x10 flag is set the format of the payload is as follows:  
**only A single byte which is interperated as:**

*   0x1 == tell the server that you are offline
*   0x2 == tell the server that you are online
*   0x4 == start monitoring this client
*   0x8 == stop monitoring this client

clients **must** ommit the reciepient field if either 0x1 or 0x2 is set in the payload since it will not be used by the server. This is the only senario that the recipient field can be ommitted

for monitoring, if the monitored client goes offline or comes online, you the one monitoring him/her must prepare for a message in the following format:

*   followed by another byte(must be treated as an opcode and not as bitmask) which can be 0x1(offline) or 0x2(online)
*   an array of bytes if this array has 8 or less bytes it will be as usual be inteperated as a number otherwise a utf8 encoded text

You wont recieve this message if you are offline.

  
see below for what offline/online really mean in pairapp

#### Hint format

The hint is just a plain text in the following format:*   unixTimestamp followed by a colon(:) followed by the raw text 'sync'
unixTimestamp is a long integer containing the date the message was recieved on the server. (not when you recieved it). example hint can be: 1039383838:sync the hint format is likely going to evolve heavily so you must keep an eye on it

#### In Pairapp offline just as online has two meanings

Depending on the context, being offline can either mean you are inactive(app running in background) or completely disconnected from the server. The same applies to online. being online can either mean being active(app is opened) or just connected to the server.